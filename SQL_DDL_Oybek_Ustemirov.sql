create database alpinismClub;

create table mount (
mount_id bigserial PRIMARY key,
peak varchar(255) not null,
company_id int CHECK (company_id > 0)
);

insert into mount (peak, company_id)
values ('Makalu', 1), 
       ('Manaslu', 2)
     
alter table mount
add column record_ts date;

UPDATE mount
SET record_ts = current_date


create table "date" (
date_id bigserial PRIMARY key,
mount_id int not null REFERENCES mount,
begining_date date,
end_date date
);

insert into "date" (mount_id, begining_date, end_date)
values (1,'2023-11-13', '2023-11-26'),
       (2, '2023-12-01', '2023-12-15')

alter table "date"
add column record_ts date;

UPDATE "date"
SET record_ts = current_date

       
       
create table "zone" (
zone_id bigserial PRIMARY key,
"name" varchar(255) not null,
country_id int not null REFERENCES country
)

insert into "zone" (zone_id, "name", country_id) 
values (1, 'Mineralniye Vodi', 1),
       (2, 'Himalai', 2)

alter table "zone"
add column record_ts date;

UPDATE "zone"
SET record_ts = current_date       
       
create table country (
country_id bigserial PRIMARY key,
"name" varchar(255) not null
);

insert into country(country_id, "name")
values (1, 'Spain'),
       (2, 'China')
       
alter table country
add column record_ts date;

UPDATE country
SET record_ts = current_date         


create table company_host (
company_id bigserial PRIMARY key,
company_name varchar(255) not null,
founding_date date NOT NULL
);

insert into company_host (company_name, founding_date)
values ('Vverh', '2002-12-15'),
       ('DoIt', '2005-08-23')
       
alter table company_host
add column record_ts date;

UPDATE company_host
SET record_ts = current_date 

       
create table company_country_id (
company_id int not null REFERENCES company_host,
country_id int not null REFERENCES country
)

insert into company_country_id (company_id, country_id)
values (1, 1),
       (2, 2)
       
alter table company_country_id
add column record_ts date;

UPDATE company_country_id
SET record_ts = current_date 

create table climber (
climber_id bigserial PRIMARY key,
climber_name varchar(255) not null,
address_id int not null REFERENCES address,
"age" date CHECK ("age" >= 18)
)


insert into climber (climber_id, climber_name, address_id, "age")
values (1, 'Alberto Gines', 1, 18),
       (2, 'Oriane Bertone', 2, 18)
       
       
alter table climber
add column record_ts date;

UPDATE climber
SET record_ts = current_date 

create table address(
address_id bigserial PRIMARY key,
climber_address varchar(255) not null
)

insert into address (address_id, climber_address)
values (1, 'Spain, MAvda. Andalucía 58'),
       (2, 'Germany, Dortmund street 23-34')

alter table address
add column record_ts date;

UPDATE address
SET record_ts = current_date 
       
       
create table peak(
peak_id bigserial PRIMARY key,
peak_name varchar not null,
height decimal CHECK (neight > 0),
zone_id int not null REFERENCES "zone"
)

insert into peak (peak_id, peak_name, height, zone_id)
values (1, 'Makalu', 8.465, 1),
       (2, 'Manaslu', 8.163, 2)

alter table peak
add column record_ts date;

UPDATE peak
SET record_ts = current_date 
       
       
create table route (
route_id bigserial PRIMARY key,
levet_of_dificulty varchar(255),
peak_id int not null REFERENCES peak
)

insert into route (route_id, levet_of_dificulty, peak_id)
values (1, 'middle', 1),
       (2, 'high', 2)

alter table route
add column record_ts date;

UPDATE route
SET record_ts = current_date 





